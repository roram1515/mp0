import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * 
 * @author Sathish Gopalakrishnan This class maintains information about a biker
 *         (in the Tour de France). It contains the biker's name and a map <yy,
 *         speed>. The map stores the biker's average speed for year yy.
 * 
 */
public class Biker {
    private String lastName;
    private String firstName;

    // This map maintains the average speed for different years.
    // The key is the year and the value is the biker's speed for that year.
    // Also notice the use of the Integer and Double classes in place of
    // the primitive int and double types.
    // This is a SortedMap, so information can be retrieved in sorted order
    // of years.
    private SortedMap<Integer, Double> averageSpeeds;

    // this represents the number of years this biker has
    // participated in the Tour de France
    private int numEvents;

    /**
     * Constructor to create a new Biker object.
     * 
     * @param lastName
     *            The biker's last name.
     * @param firstName
     *            The biker's first name.
     * @param year
     *            A year the biker participated in the Tour de France.
     * @param avgSpeed
     *            The biker's average speed in the Tour de France in the given
     *            year.
     */
    public Biker(String lastName, String firstName, int year, double avgSpeed) {
        // Notice the use of the keyword _this_.
        this.lastName = lastName;
        this.firstName = firstName;

        // Set numEvents to 1
        numEvents = 1;

        averageSpeeds = new TreeMap<Integer, Double>();

        // Notice that we are passing int and double
        // in place of Int and Double.
        // This is an example of _autoboxing_ in Java.
        averageSpeeds.put(year, avgSpeed);
    }

    /**
     * Add biker statistics for another year at the Tour de France.
     * 
     * @param year
     *            The year for the new statistics.
     * @param avgSpeed
     *            The biker's average speed in the given year.
     */
    public void addPerformanceStats(int year, double avgSpeed) {
        // add a new entry only if there is no duplication
        // otherwise silently ignore
        if (!averageSpeeds.containsKey(year)) {
            // add the new data
            averageSpeeds.put(year, avgSpeed);
            // increment numEvents
            ++numEvents;
        }
    }

    /**
     * Return the biker's full name.
     * 
     * @return the biker's full name.
     */
    public String getName() {
        return new String(firstName + " " + lastName);
    }

    /**
     * A method to identify the best improvement that the biker had at the Tour
     * de France. If the biker's speed was x in year yy1 at the Tour de France
     * and y in year yy2 (yy2 > yy1, and yy2 was the next appearance after yy1)
     * then the improvement is y-x. This method returns the best improvement for
     * the biker.
     * 
     * @return The best improvement for the biker in the Tour de France. If
     *         there was never an improvement or if the biker participated only
     *         once then the improvement is 0.
     */
    public double getBestGain() {
        // TODO this is where you must add your code
        // the return statement is bogus for now
        // You need to implement this method correctly.
    	// Check to see how many key/speeds there are in the averagespeeds map
    	// If there are more than one key/speeds then you can check
    	
    	
    	
    	// set my return variable difference to zero
    	// initialize another difference1 variable to check the best difference 
        double difference = 0;
    	double difference1 = 0;
    	
    	//if size of averageSpeeds is more than one, then check for gain
    	//if not then I return zero for no gain
    	if (averageSpeeds.size() > 1)
    	{
    		// Set up my iterator and store the first speed in variable speed
    		Iterator<Integer> itr = averageSpeeds.keySet().iterator();
    		
    		int pyear = itr.next();
    		double speed = averageSpeeds.get(pyear);
    		//I then go through the rest of the class and compare values with the first speed
    		while (itr.hasNext())
    		{
    			pyear = itr.next();
    			// store the next value in new_speed
    			double new_speed = averageSpeeds.get(pyear);
    			//Compare my first value to my second value(consecutive values)
    			if ( new_speed > speed)
    			{
    				// If there is an improvement then find gain
    				difference = new_speed - speed;
    				// Replace the initial value with the next value to move on
    				speed = new_speed;
    				//if there is another gain that is better then replace the gain just calculated
    				if (difference1 > difference)
    				{
    					difference = difference1;
    				}
    			}
    			
    			speed = new_speed;
    			
    			difference1 = difference;
    		}
    		// return the biggest difference
    		return difference;
    		
    	}
    	else
    	{
    	return difference;
    	}
    	
    }
    	

    /**
     * Returns the median speed among the biker's appearances at the Tour de
     * France.
     * 
     * @return The median speed from the set of the biker's appearances.
     */
    public double getMedianSpeed() {
        // TODO: return the median speed for this biker across the years
        // the return statement is bogus for now.
        // You need to implement this method correctly.
    	
    	// Set up a new ArrayList where I can store my speeds
    	ArrayList<Double> medspeed = new ArrayList<Double>();
    	// Set up my iterator
    	Iterator<Integer> itr = averageSpeeds.keySet().iterator();
    	//double answer_median = 0;
    	
    	//While statement keeps going till there is no entries left in averageSpeeds
    	while (itr.hasNext())
    	{
    		// Store the key "year" in variable year for later access
    		int year = itr.next();
    		//Access the averageSpeeds speed entry and store it in my ArrayList
    		medspeed.add(averageSpeeds.get(year));
    		
    	}
    	//Sort my ArrayList and get its size to calculate median
    	Collections.sort(medspeed);
    	int size_var = medspeed.size();
    	// I check to see if the size is odd and if it is then get the middle index and return
    	if (size_var %2 !=0)
    	{
    		int median_index = ((size_var + 1)/2)-1;
    		double answer_median = medspeed.get(median_index);
    		return answer_median;
    	}
    	// If the size is even then i take the average of the two middle entries in my list
    	else{
    		int median_index1 = size_var/2;
    		int median_index2 = median_index1 - 1;
    		double answer_median = (medspeed.get(median_index1)+medspeed.get(median_index2))/(2.0);
    		return answer_median;
    	}
    		
    		
    	
    	
    	
    	
    	
        //return answer_median;
    }

    /**
     * Returns the biker's speed given a participation.
     * 
     * @param yy
     *            The year for which the speed is desired.
     * @return The biker's average speed in year yy. If the biker did not
     *         participate in year yy or if the year is invalid then return 0.
     */
    public double getSpeedForYear(int yy) {
        if (yy < 0)
            return 0.0;
        if (averageSpeeds.containsKey(yy)) {
            return averageSpeeds.get(yy);
        } else
            return 0.0;
    }

}
