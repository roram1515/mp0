import java.io.*;
import java.util.*;

/**
 * This is the main class for the Tour de France program. If has the main( )
 * method that reads biker information from a file and then performs some
 * computations with the data.
 * 
 * @author Sathish Gopalakrishnan
 * 
 */
public class TdFMain {

    /**
     * This is the main( ) method.
     * 
     * @param args
     *            We do not use this parameter at this point.
     */
    public static void main(String[] args) {

        // The FileInputStream to open and read from the file that
        // has the Tour de France data.
        FileInputStream tdfStream;

        // This map maintains the relationship between the
        // name of the biker and the object that holds the
        // biker's data.
        Map<String, Biker> allBikers = new TreeMap<String, Biker>();

        // Let us try to open the data file.
        // The file name is hardcoded, which is not elegant.
        // Suffices for now.
        try {
            tdfStream = new FileInputStream("tdf.txt");
        } catch (FileNotFoundException e) {
            // If, for some reason, the file was not found,
            // then throw an exception.
            // The file is however included in the git repo
            // so this should not happen.
            throw new RuntimeException(e);
        }

        // We have opened the file.
        // Let us try to read data.
        try {
            // We will use a BufferedReader to read the data from the file.
            BufferedReader tdfReader = new BufferedReader(
                    new InputStreamReader(tdfStream));

            // We will read one line at a time and then split it.
            // The format for tdf.txt is as follows:
            // - Column 1: Year
            // - Column 2: Average Speed
            // - Column 3: Biker's last name
            // - Column 4: Biker's first name
            // tdf.txt contains real data. It is also noisy like real data.
            // Some of the names have formatting issues but we have left
            // things as is.
            String line;

            // Read each line of the file until there is nothing left to read.
            while ((line = tdfReader.readLine()) != null) {

                // Split the line into columns using the split( )
                // method for Strings.
                String[] columns = line.split(",");

                // After the split, we should have the following (as Strings):
                // - columns[0] contains the year,
                // - columns[1] contains the average speed,
                // - columns[2] contains the last name,
                // - columns[3] contains the first name.

                // Is this biker already in our list?
                // If so then we do not have to create a new biker.
                // We only have to add an entry to the existing biker.
                // We will use the full name to search allBikers.
                String key = columns[3] + columns[2]; // this is the full name

                // If search is successful then add stats
                if (allBikers.containsKey(key)) {
                    allBikers.get(key).addPerformanceStats(
                            Integer.parseInt(columns[0]),
                            Double.parseDouble(columns[1]));

                    // System.out.println("Added data to biker "+allBikers.get(
                    // key ).getName( ));
                } else {
                    // Let us now create a new Biker
                    Biker newBiker = new Biker(columns[2], columns[3],
                            Integer.parseInt(columns[0]),
                            Double.parseDouble(columns[1]));

                    // Now we add this biker to allBikers.
                    allBikers.put(key, newBiker);

                    // System.out.println("Created new biker "+newBiker.getName()
                    // );
                }

            }
            tdfReader.close();
            tdfStream.close();
        } catch (Exception e) {
            // If, for any reason, we had some problems reading data...
            throw new RuntimeException(e);
        }

        // for each entry in allBikers:
        // print the best gain for the biker.
        // The best gain is defined as the maximum improvement in
        // speed between successive entries at the Tour de France.
        // This does not have to be between consecutive years;
        // entries with a gap (no racing) between the years is okay.
        for (Map.Entry<String, Biker> currentEntry : allBikers.entrySet()) {
            Biker currentBiker = currentEntry.getValue();

            // How to print formatted strings
            // Note the use of String.format( )
            System.out.println(String.format("%-30s: %s",
                    currentBiker.getName(), currentBiker.getBestGain()));
        }

        // TODO: Compute the median speed across all the entries.
        double medianSpeed = 0;
        int big_size = 0;
        int myYear = 0;
        
       
        // Your code for this should go here and should set the correct value in
        // medianSpeed.
        // Set up an ArrayList to store all my speed entries for the bikers
        ArrayList<Double> all_speeds = new ArrayList<Double>();
        // Go through the Map allBikers to access the Biker class
        for (Map.Entry<String, Biker> currentEntry : allBikers.entrySet()) {
        	//Store the Biker class found in allBikers into my Biker currentBiker
            Biker currentBiker = currentEntry.getValue();
            // Go through the various years of the event and find the corresponding speed for the biker
            for (myYear = 2005 ;myYear <= 2012 ; myYear++ )
            {
            	//Biker currentBiker = currentEntry.getValue();
            	// I use the getSpeedforYear method to get speed
            	// I check to see if the biker participated in the given year
            	// If not, then I do not want a 0 value in my ArrayList and thus skip it
            	
            	if (currentBiker.getSpeedForYear(myYear) != 0)
            	{
            		
            		all_speeds.add(currentBiker.getSpeedForYear(myYear));
            	}
            	
            }
        }
            
        // I sort my ArrayList and get the size of it
            Collections.sort(all_speeds);
            //System.out.println(all_speeds);
            
            big_size = all_speeds.size();
            // Check to see if the size is odd and if it is then access the middle index
            // Store that value in medianSpeed
            if (big_size % 2 != 0)
            {
            	int median_index = ((big_size + 1)/2)-1;
            	medianSpeed = all_speeds.get(median_index);
            }
            // If the size is even, then I take average of the two middle indexes
            // I then store that value in mediaSpeed
            else{
            	int median_index1 = big_size/2;
            	int median_index2 = median_index1 -1;
            	medianSpeed = (all_speeds.get(median_index1) + all_speeds.get(median_index2))/(2.0);
            }
        
        // I print out the average
        System.out.println("\nThe median speed at the Tour de France is "
                + medianSpeed);

        // TODO: Compute the median of medians.
        double medianOfMedians = 0;
        // For each biker, compute the median speed. This will result in a list
        // of
        // median speeds. Now determine the median of this list.
        // Store the result in medianOfMedians.
        // Your code should go here.
        
        // Set up a new array list to store my median values for each biker
        // Ill then iterate through the allBikers map and access the Biker class in it
        ArrayList<Double> median_of_median = new ArrayList<Double>();
        for (Map.Entry<String, Biker> currentEntry : allBikers.entrySet()) {
        	
        	// I will store that biker class in the biker currentBiker variable
            Biker currentBiker = currentEntry.getValue();
            
            // Use the getMedianSpeed method on currentBiker to access the bikers average speed
            // Add the medianSpeed into my ArrayList
            median_of_median.add(currentBiker.getMedianSpeed());
        }
        
        // Sort the median_of_median ArrayList and find its size
        Collections.sort(median_of_median);
        int new_size = median_of_median.size();
        //If the size is odd then access the middle index and store in variable to return
        if (new_size % 2 != 0)
        {
        	int new_median_index = ((new_size+1)/2)-1;
        	medianOfMedians = median_of_median.get(new_median_index);
        	
        	
        }
        // if the size is even, then take the average of the two middle entries
        // store that average in variable to return
        
        else{
        	int new_median_index1 = new_size/2;
        	int new_median_index2 = new_median_index1 - 1;
        	medianOfMedians = (median_of_median.get(new_median_index1)	+ median_of_median.get(new_median_index2))/(2.0);
        }
        
        // Print out the median of medians
        System.out.println("\nThe median of medians at the Tour de France is "
                + medianOfMedians);
    }
}